import xml.etree.ElementTree as ET

import collections
from itertools import chain

class OrderedDefaultdict(collections.OrderedDict):
    """ A defaultdict with OrderedDict as its base class. """

    def __init__(self, default_factory=None, *args, **kwargs):
        if not (default_factory is None or callable(default_factory)):
            raise TypeError('first argument must be callable or None')
        super(OrderedDefaultdict, self).__init__(*args, **kwargs)
        self.default_factory = default_factory  # called by __missing__()

    def __missing__(self, key):
        if self.default_factory is None:
            raise KeyError(key,)
        self[key] = value = self.default_factory()
        return value

    def __reduce__(self):  # Optional, for pickle support.
        args = (self.default_factory,) if self.default_factory else tuple()
        return self.__class__, args, None, None, iter(self.items())

    def __repr__(self):  # Optional.
        return '%s(%r, %r)' % (self.__class__.__name__, self.default_factory, self.items())


# Two functions do all the work
def work_with_section(section, paragraphs, tag=""):
    children = [section[x] for x in range(len(section))]
    for child in children:
        if child.tag in ("title", "label"):
            
            text = child.text
            if text is None:
                if len(list(child.getchildren())) > 0:
                    text = child[0].text
            if text is not None:
                if not tag:
                    tag = text
                else:
                    tag += "|" + text
        
        if child.tag == "p":
            specital_paragraph = False
            paragraph_children = child.getchildren()
            for p_child in paragraph_children:
                if p_child.tag in ("fig", "table-wrap"):
                    paragraphs = work_with_section(p_child, paragraphs, tag)
                    specital_paragraph = True
            if not specital_paragraph:
                paragraph = "".join(child.itertext()).strip()
            else:
                parts = ([child.text] + 
                         list(chain(*([c.text, c.tail] for c in child.getchildren() if c.tag not in ("fig", "table-wrap")))) + 
                         [child.tail])
                paragraph = ''.join(filter(None, parts)).strip()
            if tag:
                paragraphs[tag].append(paragraph)
            else:
                paragraphs["Introduction"].append(paragraph)
        else:
            paragraphs = work_with_section(child, paragraphs, tag)
    return paragraphs


def parse_article_into_paragraphs(path_to_xml):        
    paragraphs = OrderedDefaultdict(lambda: [])
    tree = ET.parse(str(path_to_xml))

    abstract = tree.find(".//abstract")
    if not abstract:
        abstract = tree.find(".//Abstract")
    if abstract:
        abstract = "".join(abstract[0].itertext())
        paragraphs["Abstact"] = [abstract]

    sections = filter(None, [tree.find(".//body"), tree.find(".//floats-group")])
    for section in sections:
        work_with_section(section, paragraphs)
    return paragraphs
