from Bio import Entrez
import re

class Searcher:
    def __init__(self, api_key="", pubmed_email="vsshikov@gmail.com", pubmed_db="pmc"):
        """
        This is a Searcher module. It is used to search for papers in the publishers API.
        
        Args:
            - pubmed_email: str - pubmed email used to surf pubmed database
        """
        self.pubmed_email = pubmed_email
        self.api_key = api_key
        self.pubmed_db = pubmed_db
        self.doi_regexp = re.compile('\b(10[.][0-9]{4,}(?:[.][0-9]+)*/(?:(?!["&\'<>])\S)+)\b')
    
    def __call__(self, text_input, outfolder):
        text_input = text_input.strip()
        try:
            data, pmc = self._pubmed_search(search_query=text_input, db=self.pubmed_db)
            outfile = f"{outfolder}/{pmc}.xml"
            with open(outfile, "w") as fl:
                fl.write(data)
            return outfile
        except FileNotFoundError as e:
            print(e)
        
    
    def _pubmed_search(self, search_query, db="pmc", papers_count=1):
        """
        Return list with top PMC IDs by query and download papers from Id list
        """
        # getting search results for the query
        handle = Entrez.esearch(
            db=db,
            term=search_query,
            retmax=papers_count,
            usehistory="y",
            prefix="xlink",
        )
        search_results = Entrez.read(handle)
        handle.close()
        idlist = search_results["IdList"]  # list with PMC IDs

        if not len(idlist):
            raise FileNotFoundError("No papers where found")

        for pmc in idlist:
            ### take a paper xml
            handle = Entrez.efetch(db=db, id=pmc, rettype="full", retmode="xml")

            # save xml
            xml_data = handle.read()#.decode("utf-8")
        return xml_data, pmc
