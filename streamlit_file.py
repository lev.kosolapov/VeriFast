from paragraph_parser import *
from citation_parser import *
from searcher import *

from nn import SemanticSearcher
import streamlit as st
from annotated_text import annotated_text
import base64
import fitz

html = """
  <style>
    .reportview-container {
      flex-direction: row-reverse;
    }

    header > .toolbar {
      flex-direction: row-reverse;
      left: 1rem;
      right: auto;
    }

    .sidebar .sidebar-collapse-control,
    .sidebar.--collapsed .sidebar-collapse-control {
      left: auto;
      right: 0.5rem;
    }

    .sidebar .sidebar-content {
      transition: margin-right .3s, box-shadow .3s;
    }

    .sidebar.--collapsed .sidebar-content {
      margin-left: auto;
      margin-right: -21rem;
    }

    @media (max-width: 991.98px) {
      .sidebar .sidebar-content {
        margin-left: auto;
      }
    }
  </style>
"""
st.markdown(html, unsafe_allow_html=True)

st.markdown(
    """
    <style>
    [data-testid="stSidebar"][aria-expanded="true"] > div:first-child {
        width: 650px;
    }
    [data-testid="stSidebar"][aria-expanded="false"] > div:first-child {
        width: 650px;
        margin-left: -100px;
    }
    </style>
    """,
    unsafe_allow_html=True,
)


if 'button_clicked' not in st.session_state:
    st.session_state.button_clicked = False
if 'article' not in st.session_state:
    st.session_state.article = None


parts = [f"[{i}]" for i in range(1, 20)]

def highlight_parts(path_to_file, parts_list, format):
    # Not used in xml
    if format == "pdf":
        doc = fitz.open(path_to_file)
        for text in parts_list:
            for page in doc:
                text_instances = page.searchFor(text)

                for inst in text_instances:
                    highlight = page.addHighlightAnnot(inst)
                    highlight.update()
        new_path = path_to_file.replace(".pdf", "_highlight.pdf")
        doc.save(new_path, garbage=4, deflate=True, clean=True)
        return new_path
    else:
        return path_to_file


def generate_annotated_text(text, citation_names):
    relevant_citation_names = list(filter(lambda el: el in text, citation_names))
    if not relevant_citation_names:
        return text
    relevant_citation_names = sorted(relevant_citation_names, key=lambda citation: text.find(citation))
    final_text = []
    for cname in relevant_citation_names:
        tuple_to_insert = (cname, "", "#FFFF75")
        splitted = text.split(cname)
        i = 0
        while i < len(splitted)-1:
            text_chunk = generate_annotated_text(splitted[i], relevant_citation_names)
            if type(text_chunk) == list:
                final_text.extend(text_chunk)
            else:
                final_text.append(text_chunk)
            final_text.append(tuple_to_insert)
            i += 1
        text = splitted[-1]
    final_text.append(text)
    return final_text


def show_article(file_path, citation_names, format):
    if format == "pdf":
        with open(file_path,"rb") as f:
              base64_pdf = base64.b64encode(f.read()).decode('utf-8')
        pdf_display = f'<embed src="data:application/pdf;base64,{base64_pdf}" width="100%" height="1000" type="application/pdf">'
        st.markdown(pdf_display, unsafe_allow_html=True)
    elif format == "xml":
        paragraphs = parse_article_into_paragraphs(file_path)
        subtitles = set()
        for t, ps in paragraphs.items():
            for i, subtitle in enumerate(t.split("|")):
                if subtitle not in subtitles:
                    st.write(f"{'#'*(i+1)} {subtitle}")
                    subtitles.add(subtitle)

            # Weirdly, if there is only one paragraph,the value in the dictionary is not list with one item, but string.
            if type(ps) == list:
                for p in ps:
                    text = generate_annotated_text(p, citation_names)
                    annotated_text(*text)
                    st.write("")
            else:
                st.write(ps)

def show_paragraphs(sentence, source, paragraph_space):
    source_xml = Searcher(pubmed_db="pmc")(source, "xmls")
    input_paragraphs = parse_article_into_paragraphs(source_xml)
    output_paragraphs = SemanticSearcher()(sentence, input_paragraphs)
    with paragraph_space:
        for section, text in output_paragraphs.items():
            exp = st.expander(section, False)
            if type(text) == str:
                text = [text]
            for p in text:
                exp.write(p)

def first_screen():
    global article_pdf
    st.title("Hello, user, I hope you are excited to read your paper comfortably!")
    with st.form(key='my_form'):
        text_input = st.text_input(label='Enter the article url:')
        article_pdf = st.file_uploader("Load pdf: ", type=['pdf', "xml"])
        st.session_state.button_clicked = st.form_submit_button(label='Submit')
    return article_pdf

def second_screen(article_file, citation_buttons, paragraph_space):
    if article_file is not None:

        # Display the article
        fm = article_file.name.split(".")[-1]
        if fm == "pdf":
            save_path = './pdfs/'+ article_file.name
            with open(save_path, "wb") as fl:
                fl.write(article_file.getbuffer())
        elif fm == "xml":
            save_path = './xmls/'+ article_file.name
            with open(save_path, "wb") as fl:
                fl.write(article_file.getbuffer())
        else:
            st.write("Couldn't recognise file format")
            return None
        new_path = highlight_parts(save_path, parts, format=fm)
        citations = extract_citations(new_path)
        cnames = [el[0] for el in citations]

        show_article(new_path, citation_names=cnames, format=fm)

        # Display the citation buttons
        with citation_buttons:
            for citation_name, citation_source, citation_sentence in citations:
                st.markdown("<style> div.stButton > button:first-child {background-color: #FFFF75;}</style>", unsafe_allow_html=True)
                b = st.button(label=citation_name)
                if b:
                    show_paragraphs(citation_sentence[0], citation_source, paragraph_space)
    else: 
        first_screen()
        
    
if not st.session_state.button_clicked:
    st.session_state.article = first_screen()
else:
    citation_buttons, paragraph_space = st.sidebar.columns([1,2])
    second_screen(st.session_state.article, citation_buttons, paragraph_space)
    
