#!/usr/bin/env python3
import time
import logging
import json
from flask import Flask, request, jsonify
from parser import Parser
from searcher import Searcher
from nn import SemanticSearcher

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

app = Flask(__name__)

# Loading config.json (file with api-keys, emails and etc, all info needed for search)
with open("config.json") as fp:
    config = json.load(fp)
    
# Initiate searcher
logger.info("INIT SEARCHER")
searcher = Searcher(**config['searcher'])

# Initiate parser
logger.info("INIT PARSER")
parser = Parser()

# Initalize NN
logger.info("INIT NN")
nn = SemanticSearcher(**config['nn'])

logger.info("DONE")

@app.route('/search')
def respond():
    # obtain search request from browser
    search_request = request.json['input']
    # search for article
    logger.info(f"SEARCH QUERY: {search_request}")
    data, data_format = searcher(search_request)
    # parse article
    parsed_data = parser(data, data_format, parse_citations=True)
    citations = []
    for paper in parsed_data['citation']:
        paper_data, paper_data_format = searcher(paper['doi'])
        parsed_paper_data, _ = parser(paper_data, paper_data_format, parse_citations=False)
        citations.append(parsed_paper_data)
        parsed_data['citation']
    return parsed_data, citations
    

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=7777)
