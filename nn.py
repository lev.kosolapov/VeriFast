
#!/usr/bin/env python3
from sentence_transformers import SentenceTransformer, util

def get_sections_and_sentence(parsed_paper, sentence_id):
    section_id, paragraph_id, sentence_id, cit_label = sentence_id
    sentence = parsed_paper['text'][section_id][paragraph_id][sentence_id]
    sections = parsed_paper['citations']['papers'][cit_label]['text']
    return sentence, sections, cit_label


class SemanticSearcher:
    def __init__(self, model='msmarco-distilbert-base-tas-b', device='cpu', top_k=1):
        self.top_k = top_k
        self.model = SentenceTransformer('msmarco-distilbert-base-tas-b', device=device)
        
        
    def __call__(self, paper, cit_label):
        sentences = []
        ids = []
        for section_id, paragraph_id, sentence_id, t_cit_label in parsed_paper['citations']['sentence_ids']:
            if t_cit_label == cit_label:
                sentences.append(parsed_paper['text'][section_id][paragraph_id][sentence_id])
                ids.append((section_id, paragraph_id, sentence_id))
        selected_paragraphs = [[] for i in ids]
        query_embedding = self.model.encode(sentences, convert_to_tensor=True)
        for section in paper['papers']['text']:
            paragraphs = ["".join(p) for p in section['section_text']]
            passage_embeddings = self.model.encode(paragraphs, convert_to_tensor=True)
            search_results = util.semantic_search(
                    query_embedding,
                    passage_embedding,
                    top_k=self.top_k,
                    score_function=util.dot_score
            )
            for id_, paragraph, search_res in zip(selected_paragraphs, search_results)
                paragraph.append((id_,  search_res))
        paper['papers'][cit_label]['selected_paragraphs'] = selected_paragraphs
        return paper

#     def __call__(self, paper):
#         # 1. add `selected_paragraphs` as a list of ids of citation sentences
#         # 2. for each paper, for each section - run semantic search
#         # 3. for each search result
#         for sentence_id in parsed_paper['citations']['sentence_ids']:
#             sentence, sections, cit_label = get_sections_and_sentence(parsed_paper, sentence_id)
#             if 'selected_paragraphs' not in parsed_paper['citations']['papers'][cit_label]:
#                 parsed_paper['citations']['papers'][cit_label]['selected_paragraphs'] = [sentence_id]
#             else:
#                 parsed_paper['citations']['papers'][cit_label]['selected_paragraphs'].append(sentence_id)
#         for cit_label, paper in parsed_paper['citations']['papers'].values():
#             sentences = [
#                 parsed_paper['text'][section_id][paragraph_id][sentence_id] 
#                 for (section_id, paragraph_id, sentence_id, _) in paper['selected_paragraphs']
#             ]
#             sections = parsed_paper['citations']['papers'][cit_label]['text']
#             query_embedding = model.encode(sentences, convert_to_tensor=True)
#             selected_paragraphs = []
#             for section in sections:
#                 paragraphs = ["".join(paragraph) for paragraph in section['section_text']]
#                 passage_embeddings = model.encode(paragraphs, convert_to_tensor=True)
#                 search_results = util.semantic_search(
#                     query_embedding,
#                     passage_embedding,
#                     top_k=top_k,
#                     score_function=util.dot_score
#                 )
#                 selected_paragraphs.append(search_results)
#             for i, element in enumerate(paper['selected_paragraphs']):
#                 element.append([(j, res) for j, res in enumerate(search_results)])
#         return paper