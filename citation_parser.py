from paragraph_parser import parse_article_into_paragraphs
import xml.etree.ElementTree as ET
from lxml.etree import tostring

from collections import defaultdict
from itertools import chain

from pathlib import Path

# Demonstration

def parse_references(path_to_xml):
    tree = ET.parse(str(path_to_xml))
    references = []
    sections = tree.findall(".//element-citation")
    
    for section in sections:
        print(section.text)
        current = {}
        children = [section[x] for x in range(len(section))]
        for ch in children:
            if ch.tag == 'article-title':
                current['article-title'] = ch.text
            if ch.tag == 'pub-id':
                current['pub-id'] = ch.text
        if current!={}:
            references.append(current)
        current = {}
    return references


# def extract_citations(path_to_file):
#     citations = []
#     for i in range(1, 100):
#         citation_name = f"[{i}]"
#         citation_sentence = "We speculate that SOX2 might be involved in several steps during lung SCC progression. First, SOX2 is capable of transforming and conferring tumor-initiating properties to human lung squamous cells, as we demonstrated. Second, SOX2 contributes to establishment of a stem cell-like poorly-differentiated phenotype in advanced lung SCC, as is also suggested for SOX2 in basal-like breast cancer."
#         citation_source = "Whatever"
#         citations.append([citation_name, citation_sentence, citation_source])
#     return citations

def extract_citations(path):
    tree = ET.parse(str(path))
    citations = []
    for el in tree.findall(".//xref[@ref-type='bibr']"):
        xxx = el.attrib["rid"]
        ref = tree.find(f".//ref[@id='{xxx}']")
        article_name = ref.find(".//article-title").text
        if el.text not in [c[0] for c in citations]:
            citations.append([el.text, article_name])

    paragraphs = parse_article_into_paragraphs(path)
    pps = []
    for val in paragraphs.values():
        pps.extend(val)

    complete_citations = []
    for citation in citations:
        cname = citation[0]
        ss = []
        for paragraph in pps:
            if cname in paragraph:
                sentences = paragraph.split(".")
                for sentence in sentences:
                    if cname in sentence:
                        ss.append(sentence.replace(cname, ""))
        complete_citations.append([citation[0], citation[1], ss])
    return complete_citations